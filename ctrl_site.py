import jinja2
from aiohttp import web
from functools import lru_cache

from db import Session, User, Auth
from util import hash
from util.misc import gen_uuid
import settings

def create_app(*, serve_static = False):
	app = App()
	app['stats'] = None
	app.router.add_get('/', page_index)
	app.router.add_get('/stats', page_stats)
	app.router.add_get('/forgot', page_forgot)
	app.router.add_post('/forgot', page_forgot)
	app.router.add_get('/reset/{token}', page_reset)
	app.router.add_post('/reset/{token}', page_reset)
	app.router.add_get('/etc/MsgrConfig', page_msgr_config)
	app.router.add_post('/etc/MsgrConfig', page_msgr_config)
	app.router.add_get('/etc/escargot-today', page_escargot_today)
	app.router.add_get('/faq', page_faq)
	app.router.add_get('/downloads', page_downloads)
	app.router.add_get('/register', page_register)
	app.router.add_post('/register', page_register)
	app.router.add_get('/patching', page_patching)
	if serve_static:
		app.router.add_static('/static', 'static')
	app.router.add_route('*', '/{path:.*}', handle_404)
	app.jinja_env = jinja2.Environment(
		loader = jinja2.FileSystemLoader('tmpl'),
		autoescape = jinja2.select_autoescape(default = True),
	)
	return app

class App(web.Application):
	async def startup(self):
		self.loop.create_task(self.sync_stats())
	
	async def sync_stats(self):
		import asyncio
		from datetime import datetime, timedelta
		from itertools import groupby
		import stats
		
		while True:
			now = datetime.utcnow()
			five_minutes_ago = now - timedelta(minutes = 5)
			hour_cutoff = now.timestamp() // 3600 - 24
			with stats.Session() as sess:
				stat = sess.query(stats.CurrentStats).filter(
					stats.CurrentStats.key == 'logged_in',
					stats.CurrentStats.date_updated > five_minutes_ago
				).one_or_none()
				logged_in = getattr(stat, 'value', 0)
				clients_by_id = {
					row.id: row.data
					for row in sess.query(stats.DBClient).all()
				}
				by_hour = sorted([
					{
						'hour': hcs.hour,
						'hour_formatted': _format_hour(hcs.hour),
						'client_formatted': _format_client(clients_by_id.get(hcs.client_id)),
						'users_active': int(hcs.users_active),
						'messages_sent': hcs.messages_sent,
						'messages_received': hcs.messages_received,
					}
					for hcs in sess.query(stats.HourlyClientStats).filter(stats.HourlyClientStats.hour >= hour_cutoff).all()
				], key = lambda x: (-x['hour'], -x['users_active']))
				by_hour = [
					(hour_formatted, list(l))
					for hour_formatted, l in groupby(by_hour, lambda x: x['hour_formatted'])
				]
			self['stats'] = {
				'logged_in': logged_in,
				'by_hour': by_hour,
			}
			await asyncio.sleep(300)

def _format_hour(hour):
	from datetime import datetime
	dt = datetime.fromtimestamp(hour * 3600)
	return dt.strftime("%Y-%m-%d, %H:00 - %H:59 UTC")

def _format_client(client):
	if client is None:
		return "(Unknown)"
	p = client['program']
	v = client['version']
	if p == 'msn' and v.startswith('MSNP'):
		v = _guess_msn_version(int(v[4:]))
	s = "{} {}".format(p.upper(), v)
	if client['via'] != 'direct':
		s += ", {}".format(client['via'].upper())
	return s

def _guess_msn_version(dialect):
	if dialect <= 2:
		return "1.?"
	if dialect <= 4:
		return "2.?"
	if dialect <= 5:
		return "3.?"
	if dialect <= 7:
		return "4.?"
	return "?/MSNP" + str(dialect)

async def page_stats(req):
	return render(req, 'stats.html', { 'stats': req.app['stats'] })

async def page_index(req):
    downloads, mirrored = _get_downloads()
    return render(req, 'index.html', {
        'downloads': downloads,
        'mirrored': mirrored,
    })

async def page_register(req):
    errors = None
    email = None
    created_email = None
    support_old = None
    if req.method == 'POST':
        data = await req.post()
        
        valid_recaptcha = await check_recaptcha(req, data.get('g-recaptcha-response') or '')
        if not valid_recaptcha:
            errors = { 'recaptcha': "Invisible reCAPTCHA failed." }
        
        if not errors:
            email = data.get('email') or ''
            pw1 = data.get('password1') or ''
            pw2 = data.get('password2') or ''
            support_old = (data.get('support_old') == 'true')
            errors = create_user(email, pw1, pw2, support_old)
        
        if not errors:
            return web.HTTPFound('/register?created_email={}'.format(email))
    else:
        created_email = req.query.get('created_email')
    
    return render(req, 'register.html', {
        'errors': errors,
        'email': email,
        'support_old': support_old,
        'created_email': created_email,
        'recaptcha_api_key': settings.RECAPTCHA['api_key'],
    })


async def handle_404(req):
	return render(req, '404.html', status = 404)

async def check_recaptcha(req, recaptcha_response):
	if not settings.RECAPTCHA['secret_key']:
		return True
	
	import aiohttp
	async with aiohttp.ClientSession() as session:
		req = session.post('https://www.google.com/recaptcha/api/siteverify', json = {
			'secret': settings.RECAPTCHA['secret_key'],
			'response': recaptcha_response,
			'remoteip': req.remote,
		})
		async with req as resp:
			resp = await resp.json()
	return resp['success']

async def page_msgr_config(req):
	msgr_config = _get_msgr_config()
	return web.Response(status = 200, content_type = 'text/xml', text = msgr_config)

async def page_escargot_today(req):
	return render(req, 'escargot-today.html')

@lru_cache()
def _get_msgr_config():
	with open('MsgrConfigEnvelope.xml') as fh:
		envelope = fh.read()
	with open('MsgrConfig.xml') as fh:
		config = fh.read()
	return envelope.format(MsgrConfig = config)

@lru_cache()
def _get_downloads():
	import csv
	from itertools import groupby
	
	langs = {}
	with open('languages.csv') as fh:
		for row in csv.DictReader(fh):
			langs[row['code']] = row['name_english']
	
	with open('downloads.csv') as fh:
		rows = list(csv.DictReader(fh))
	
	with open('mirrored.csv') as fh:
		mirrored = { row['file'] for row in csv.DictReader(fh) }
	
	downloads = {}
	for langcode, lang_downloads in groupby(rows, key = lambda r: r['langcode']):
		lang_downloads = list(lang_downloads)
		lang_name = langs[langcode]
		if lang_name not in downloads:
			downloads[lang_name] = []
		downloads[lang_name].extend(lang_downloads)
	return sorted(downloads.items(), key = lambda pair: pair[0]), mirrored

async def page_forgot(req):
	errors = None
	email = None
	sent_to = None
	if req.method == 'POST':
		data = await req.post()
		email = data.get('email') or ''
		errors = send_password_reset(email)
		if not errors:
			return web.HTTPFound('/forgot?sent_to={}'.format(email))
	else:
		sent_to = req.query.get('sent_to')
	
	return render(req, 'forgot.html', {
		'errors': errors,
		'email': email,
		'sent_to': sent_to,
	})

async def page_reset(req):
	token = req.match_info.get('token', '')
	email = Auth.GetToken(token)
	valid_token = (email is not None)
	errors = None
	reset_success = False
	support_old = None
	
	if valid_token:
		if req.method == 'POST':
			data = await req.post()
			pw1 = data.get('password1') or ''
			pw2 = data.get('password2') or ''
			support_old = (data.get('support_old') == 'true')
			errors = change_password(email, pw1, pw2, support_old)
			if not errors:
				Auth.ClearToken(token)
				reset_success = True
		else:
			with Session() as sess:
				user = _get_user(email)
				if user:
					support_old = bool(user.password_md5)
	
	return render(req, 'reset.html', {
		'valid_token': valid_token,
		'errors': errors,
		'reset_success': reset_success,
		'support_old': support_old,
	})

async def page_faq(req):
	return render(req, 'faq.html')

async def page_downloads(req):
	return render(req, 'downloads.html')

async def page_patching(req):
	return render(req, 'patching.html')

def create_user(email, pw1, pw2, support_old):
	errors = {}
	_check_email(errors, email)
	_check_passwords(errors, pw1, pw2)
	if errors: return errors
	
	with Session() as sess:
		user = _get_user(email)
		if user is not None:
			errors['email'] = "used" #"Email already in use."
			return errors
		user = User(
			uuid = gen_uuid(), email = email, verified = False,
			name = email, message = '',
			settings = {}, groups = {}, contacts = {},
		)
		_set_passwords(user, pw1, support_old)
		sess.add(user)
	
	return errors

def send_password_reset(email):
	errors = {}
	_check_email(errors, email)
	if errors: return errors
	
	with Session() as sess:
		user = _get_user(email)
		if user is None:
			errors['email'] = "not-registered" #"Email not registered with Escargot. Did you sign up?"
			return errors
		token = Auth.CreateToken(email, lifetime = 3600)
		#sent = _send_password_reset_email(email, token)
		sent = True
		print(token)
		if not sent:
			Auth.ClearToken(token)
			errors['email'] = "not-sent" #"Email could not be sent."
	
	return errors

def change_password(email, pw1, pw2, support_old):
	errors = {}
	_check_passwords(errors, pw1, pw2)
	if errors: return errors
	
	with Session() as sess:
		user = _get_user(email)
		_set_passwords(user, pw1, support_old)
		user.verified = True
		sess.add(user)
	
	return errors

def _set_passwords(user, pw, support_old):
	user.password = hash.hasher.encode(pw)
	user.password_md5 = (hash.hasher_md5.encode(pw) if support_old else '')

def _send_password_reset_email(email, token):
	import mailjet_rest
	mj = mailjet_rest.Client(auth = (settings.MAILJET['api_key'], settings.MAILJET['secret_key']))
	ret = mj.send.create({
		'FromName': "Escargot",
		'FromEmail': 'no-reply@escargot.log1p.xyz',
		'Subject': "Escargot password reset requested",
		'Recipients': [{ 'Email': email }],
		'Text-Part': RESET_TEMPLATE.format(email = email, token = token)
	})
	return ret.status_code == 200

RESET_TEMPLATE = """
A password reset was requested for the Escargot (https://escargot.log1p.xyz) account associated with {email}.

To change your password, follow this link within the next 60 minutes:

https://escargot.log1p.xyz/reset/{token}

If you did not request a password reset, ignore this email.
""".strip()

def _get_user(email):
	with Session() as sess:
		return sess.query(User).filter(User.email == email).one_or_none()

def _check_email(errors, email):
	if not (6 <= len(email) < 60) or ('@' not in email):
		errors['email'] = "invalid" #"Invalid email."
	if '|' in email:
		errors['email'] = "pass-in-email" #"Don't put your password in the email here; that's only when you log in to MSN < 5 or 7.5"

def _check_passwords(errors, pw1, pw2):
	if len(pw1) < 6:
		errors['password1'] = "too-short" #"Password too short: 6 characters minimum."
	elif pw1 != pw2:
		errors['password2'] = "no-match" #"Passwords don't match."

def render(req, tmpl, ctxt = None, status = 200):
	tmpl = req.app.jinja_env.get_template(tmpl)
	if ctxt is None:
		ctxt = {}
	ctxt['stats'] = req.app['stats']
	content = tmpl.render(**ctxt)
	return web.Response(status = status, content_type = 'text/html', text = content)
