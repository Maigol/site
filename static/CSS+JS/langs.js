var userLang = navigator.language || navigator.userLanguage;
	userLang = userLang.split("-")[0];

var site;
switch (window.location.pathname) {
	case "/downloads":
		site = "downloads";
		break;
	case "/patching":
		site = "patching";
		break;
	case "/register":
		site = "register";
		break;
	case "/forgot":
		site = "forgot";
		break;
	default:
		site = "home";
}
if (window.location.pathname.indexOf("/reset") != -1) {
	site = "reset";
}

var rs;

function showWarn(x) {
	var contact = "crurbox@gmail.com";
	var partial = "This part of the website is not translated to your language yet. If you want to help, please contact this email: <a href='mailto:"+contact+"'>"+contact+"</a>";
	var full = "The site hasn't been translated to your language yet. If you want to help, please contact this email: <a href='mailto:"+contact+"'>"+contact+"</a>"
	var d = document.createElement('div');
	d.align = "center";
	d.style.position = "absolute";
	d.style.opacity = "0.5";
	d.style.width = "99.7%";
	d.style.backgroundColor = "orange";
	d.style.top = "5px";
	d.style.border = "2px solid #7a4f00";
	d.style.fontSize = "11px";

	switch (x) {
		case "partial":
			d.innerHTML = partial;
			break;
		case "full":
			d.innerHTML = full;
			break;
		default:
			throw new Error("No argument");
			break;
	}

	document.body.appendChild(d);
}

function getJson(file) {
	var req = new XMLHttpRequest();
	req.open('GET', '/static/json/'+file+'.json', false);

	req.send();

	if (req.status == 200) {
		if (req.response != undefined) {
			return JSON.parse(req.response);
		}
	}
	else if (req.status == 404) {
		showWarn("full");
		translate('en');
	}
}


function translate(lang) {
	var lang = getJson(lang);
	sLang = lang[site];


	if ("text" in sLang) {
		for (i in sLang.text) {
			try {
				document.getElementById(i).innerHTML = sLang["text"][i];
			} catch(e) {
				continue;
			}
		}
	}
	if ("xtras" in sLang) {
		for (t in sLang.xtras.titles) {
			document.getElementById("xtra-"+t+"-title").innerHTML = sLang.xtras.titles[t];
		}
		for (d in sLang.xtras.descs) {
			document.getElementById("xtra-"+d).title = sLang.xtras.descs[d];
		}
	}
	if ("status" in sLang) {
		for (i in sLang.status) {
			for (s in document.getElementsByClassName(i)) {
				e = ["item", "namedItem", "length"];
				if (e.indexOf(s) == -1) {
					document.getElementsByClassName(i)[s].childNodes[0].textContent = sLang.status[i];
				}
			}
		}
	}
	if ("placeholders" in sLang) {
		for (p in sLang.placeholders) {
			document.getElementById(p).placeholder = sLang.placeholders[p]
		}
	}
	if (site == "register" && document.getElementById('created_email') != null) { //Show registered account after translation was applied
		document.getElementById('created_email').innerHTML = document.getElementById('c-email-h').innerHTML;
	}
	if (site == "forgot" && document.getElementById("sent_to") != null) {
		document.getElementById("sent_to").innerHTML = document.getElementById("c-email-s").innerHTML;
	}
	if ("errors" in sLang) {
		for (e in sLang.errors) {
			if (document.getElementById(e+"-error") != null) {
				var errId = e;
				var errMsg = document.getElementById(e+"-error");
				break;
			}
		}

		for (m in sLang.errors[errId]) {
			if (errMsg.innerHTML == m) {
				errMsg.innerHTML = sLang.errors[errId][m];
			}
		}
	}
	if (site == "forgot" || site == "reset") {
		for (p in lang.register.placeholders) {
			try {
				document.getElementById(p).placeholder = lang.register.placeholders[p];
			} catch(e) {
			}
		}
		if (document.getElementById('support_old-text') != null) {
			document.getElementById('support_old-text').innerHTML = lang.register.text["support_old-text"];
		}
	}

	if (lang.unfinished) {
		if (lang["unfinished"].indexOf(rs) != -1) {
			showWarn("partial");
		}
	}
}

function inMirrored(s, m) {
	for (i in m) {
		if (m[i].indexOf(s) >= 0) {
			return true;
		}
	}
	return false;
}

function getDwnloads(lang) {
	var gP = "https://storage.googleapis.com/escargot-storage-1/public/";
	var eP = "http://storage.log1p.xyz/";

	var d = getJson('downloads');
	var m = getJson('mirrored');

	for (i in d.lang) {
		//This resets the href to avoid a link to a previously selected from being stored
		var x = document.getElementById("up-"+i);
			x.href = "#";

		if (i >= 50) {
			var x = document.getElementById("pp-"+i);
				x.href = "#";
		}
	}

	for (i in d[lang]) {
		if (i >= 50) {
			var t = d[lang][i][0];
			var a = document.getElementById("pp-"+i);
			if (inMirrored(t, m)) {
				a.href = eP+t;
			}
			else {
				a.href = gP+t;
			}
		}
	}
	for (i in d[lang]) {
		var t = d[lang][i][1];
		var a = document.getElementById("up-"+i);
		if (inMirrored(t, m)) {
			a.href = eP+t;
		}
		else {
			a.href = gP+t;
		}
	}
	document.getElementById("recommended").href = document.getElementById("pp-85").href;

	for (i in d.lang) {
		var x = document.getElementById("up-"+i);
		if (x.href.indexOf("#") != -1) {
			x.style.display = "none";
		} else {
			x.style.display = "inline-block";
		}

		if (i >= 50) {
			var x = document.getElementById("pp-"+i);
			if (x.href.indexOf("#") != -1) {
				x.style.display = "none";
			} else {
				x.style.display = "inline-block";
			}
		}
	}
}

var pShown = "none"; //Current instructions shown
function showP(ver) {
	if (pShown == ver) {
		hideP(pShown);
		pShown = "none";0 
		document.getElementById("patchnone").style.display = "block";
	} else {
		hideP("none");
		hideP(pShown);
		pShown = ver;
		document.getElementById("patch"+ver).style.display = "block";
	}
}

function hideP(ver) {
	document.getElementById("patch"+ver).style.display = "none";
}

if (site == "downloads") {
	setInterval(
		function() {
				if (window.pageYOffset > 10) {
					window.scrollTo(0, document.body.scrollHeight);
				}
				else {
					window.scrollTo(0, 0);
				}
		},
		500
	);
}

//https://stackoverflow.com/a/30319853 Wait until the DOM loads to call a function
function r(f){/in/.test(document.readyState)?setTimeout('r('+f+')',9):f()}
r(function(){
	rs = document.getElementById('id').title;
    translate(userLang);
});

var sLang = getJson(userLang);
	sLang = sLang[site];